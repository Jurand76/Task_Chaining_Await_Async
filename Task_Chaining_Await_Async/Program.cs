﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_Chaining
{
    class Program
    {
        static void Main()
        {
            async Task<int[]> Task1()
            {
                int[] array = Arrays.CreateArray10integers();
                return array;
            }
            
            async Task<int[]> Task2(int[] inputArray)
            {
                int[] array = Arrays.MultiplyArrayByNumber(inputArray, 3);
                return array;
            }

            async Task<int[]> Task3(int[] inputArray)
            {
                int[] array = Arrays.SortArrayAscending(inputArray);
                return array;
            }

            async Task<double> Task4(int[] inputArray)
            {
                double average = Arrays.CalculateAverageOfArray(inputArray);
                return average;
            }

            async Task Task5()
            {
                int[] result = await Task1();
                int[] result2 = await Task2(result);
                int[] result3 = await Task3(result2);
                double result4 = await Task4(result3);
                Arrays.PrintArrayOnConsole(result);
                Console.WriteLine();
                Arrays.PrintArrayOnConsole(result2);
                Console.WriteLine();
                Arrays.PrintArrayOnConsole(result3);
                Console.WriteLine();
                Console.WriteLine("Average : " + result4);
                Console.WriteLine();
            }

            Task returnedTask = Task5();
            returnedTask.Wait();

            Console.WriteLine();
            Console.WriteLine("Press ENTER to exit.");
            Console.ReadLine();
        }
    }
}

