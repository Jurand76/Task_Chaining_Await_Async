using NUnit.Framework;
using System;
using Tasks_Chaining;

namespace Tasks_Chaining.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Create10ElementsArray()
        {
            int[] array10Element = Tasks_Chaining.Arrays.CreateArray10integers();
            Assert.AreEqual(10, array10Element.Length);
        }

        [Test]
        public void MultiplyArrayBy3()
        {
            int[] exampleArray = new int[] { -40, -370, -10, 30, 7, 90, -180, 340, 0, -820 };
            int[] multiplyArray = new int[] { -120, -1110, -30, 90, 21, 270, -540, 1020, 0, -2460 };
            int[] multiplyArrayResult = Tasks_Chaining.Arrays.MultiplyArrayByNumber(exampleArray, 3);
            Assert.AreEqual(multiplyArrayResult, multiplyArray);
        }

        [Test]
        public void SortArrayAscending()
        {
            int[] multiplyArray = new int[] { -120, -1110, -30, 90, 21, 270, -540, 1020, 0, -2460 };
            int[] sortedArray = new int[] { -2460, -1110, -540, -120, -30, 0, 21, 90, 270, 1020 };
            int[] sortedArrayResult = Tasks_Chaining.Arrays.SortArrayAscending(multiplyArray);
            Assert.AreEqual(sortedArrayResult, sortedArray);
        }

        [Test]
        public void CalculateArrayAverage()
        {
            int[] exampleArray = new int[] { -2460, -1110, -540, -120, -30, 0, 21, 90, 270, 1020 };
            double averageExpected = -285.9;
            double averageCalculated = Tasks_Chaining.Arrays.CalculateAverageOfArray(exampleArray);
            Assert.AreEqual(averageCalculated, averageExpected);
        }

        [Test]
        public void ArrayIsEmptyArgumentNullExpression()
        {
            int[] exampleArray = new int[] { };
            Assert.Throws<ArgumentNullException>(() => Tasks_Chaining.Arrays.MultiplyArrayByNumber(exampleArray, 3));
            Assert.Throws<ArgumentNullException>(() => Tasks_Chaining.Arrays.SortArrayAscending(exampleArray));
            Assert.Throws<ArgumentNullException>(() => Tasks_Chaining.Arrays.CalculateAverageOfArray(exampleArray));
        }
    }
}